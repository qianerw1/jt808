# JT808交通部808协议

#### 介绍
实现交通部808协议获取车载GPS数据

#### 软件架构
JAVA  
Spring4  
JDK1.6+  
支持mysql与SqlServer库  

![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/200657_4454bb5b_1850741.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0525/104831_0760adc4_1850741.png "屏幕截图.png")
![输入图片说明](360%E6%88%AA%E5%9B%BE167708081119591.png)

#### 安装教程

1. 见808代码说明.doc

#### 使用说明

本地:eclipse或者idea中运行程序主入口MainApp类，即可进入图形化界面  
线上：java -jar xxxx.jar  
默认启动7611端口（可在图形化界面中修改）    

本工程中同时具备设备模拟工具可以模拟车载设备的指令交互  

另外如需要809协议的解析服务或者1078的视频服务可以私信联系我    

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)