package com.ltmonitor.service;

import java.util.List;

import com.ltmonitor.entity.GPSRealData;

public interface IRealDataService {

	public void update(GPSRealData rd);

	public GPSRealData get(String simNo);
	/**
	 * 根据sim卡号获取实时数据
	 * @param simNoList
	 * @return
	 */
	public List<GPSRealData> getRealDataList(List<String> simNoList);
	/**
	 * 更新上线时间
	 * @param simNo
	 */
	public void updateOnlineTime(String simNo);
	/**
	 * 停止线程分析服务
	 */
	public void stopService();

	public List<GPSRealData> getOnlineRealDataList();

}