package com.ltmonitor.dao;

import java.io.Serializable;

import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;

public interface IBaseDao {
    public Serializable save(Object obj);

    public void update(Object obj);

    public void saveOrUpdate(Object obj) ;
    
    public void saveOrUpdateAll(Collection entities);

    public void remove(Object obj) ;


    public List loadAll(Class clazz);

    public Object load(Class clazz, Serializable sid);


    public Session getCurrentSession();


    public Object find(String hsql);

    public Object find(String hsql, Object value);

    public Object find(String hsql, Object[] values);

    public List queryByNamedParam(final String hql, String paramName, final Object value);

    public List query(String hsql, Object[] value);
    public List query(final String hql, final Object value);

	//public void removeByFake(FakeDeletedEntity obj);

	public void removeByFake(Class clazz, Serializable id);


	public void remove(Class clazz, Serializable id);

	/**
	 * 命名查询
	 * @param queryID
	 * @param params
	 * @return
	 */
	public List queryByNamedQuery(String queryID, Object[] params);

}
