﻿package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * 每小时耗油量和每天的累计
 * 由定时器定时生成此记录
 */

@Entity
@Table(name="FuelConsumption")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)     
public class FuelConsumption extends TenantEntity
{
	public static final int STATIC_BY_HOUR = 0; //按小时统计；
	public static final int STATIC_BY_DAY = 1;
	public static final int STATIC_MONTH = 2;
	public static final int STATIC_TIME_SPAN = 3; //按时间段统计
	
	public FuelConsumption()
	{
		this.setCreateDate(new Date());
	}
	
	private int  entityId;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}


	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}

	private String intervalDescr;
	public final String getIntervalDescr()
	{
		return intervalDescr;
	}
	public final void setIntervalDescr(String value)
	{
		intervalDescr = value;
	}

	private double hour;
	public final double getHour()
	{
		return hour;
	}
	public final void setHour(double value)
	{
		hour = value;
	}
	//统计日期
	private java.util.Date staticDate = new java.util.Date(0);
	public final java.util.Date getStaticDate()
	{
		return staticDate;
	}
	public final void setStaticDate(java.util.Date value)
	{
		staticDate = value;
	}

	//统计间隔 0, 小时，1，天，2，月等等
	private int intervalType;
	public final int getIntervalType()
	{
		return intervalType;
	}
	public final void setIntervalType(int value)
	{
		intervalType = value;
	}

	//行驶里程
	private double mileage;
	public final double getMileage()
	{
		return mileage;
	}
	public final void setMileage(double value)
	{
		mileage = value;
	}

	//耗油量
	private double gas;
	public final double getGas()
	{
		return gas;
	}
	public final void setGas(double value)
	{
		gas = value;
	}

	//起始和终止的油量和里程统计
	private double gas1;
	public final double getGas1()
	{
		return gas1;
	}
	public final void setGas1(double value)
	{
		gas1 = value;
	}

	private double gas2;
	public final double getGas2()
	{
		return gas2;
	}
	public final void setGas2(double value)
	{
		gas2 = value;
	}

	private double mileage1;
	public final double getMileage1()
	{
		return mileage1;
	}
	public final void setMileage1(double value)
	{
		mileage1 = value;
	}

	private double mileage2;
	public final double getMileage2()
	{
		return mileage2;
	}
	public final void setMileage2(double value)
	{
		mileage2 = value;
	}

	

}