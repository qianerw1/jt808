package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 车组/部门类  
 * @author Administrator
 *
 */
@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "department", uniqueConstraints = { @javax.persistence.UniqueConstraint(columnNames = { "name" }) })
public class Department extends TenantEntity implements Serializable {
	private static final long serialVersionUID = -3005019099093973743L;
	
	public static final String CORP = "corporation";
	public static final String COMPANY = "company";
	public static final String DEPARTMENT = "department";
	private int entityId;
	private int parentId;
	//部门名称
	private String name;
	private String type;
	private String memNo = "";

	private String roadPermitWord;
	private String roadPermitNo;
	private String region;
	private String businessScope;
	private String assoMan;
	private String assoTel;
	private Date updateTime = new Date();
	@Transient
	private int onlineNum ;

	@Transient
	private int totalNum;

	public Department() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "depId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}

	@Column(name = "name", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMemNo() {
		return this.memNo;
	}

	public void setMemNo(String memNo) {
		this.memNo = memNo;
	}


	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}


	public String getRoadPermitWord() {
		return this.roadPermitWord;
	}

	public void setRoadPermitWord(String roadPermitWord) {
		this.roadPermitWord = roadPermitWord;
	}

	public String getRoadPermitNo() {
		return this.roadPermitNo;
	}

	public void setRoadPermitNo(String roadPermitNo) {
		this.roadPermitNo = roadPermitNo;
	}


	public String getBusinessScope() {
		return this.businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	public String getAssoMan() {
		return this.assoMan;
	}

	public void setAssoMan(String assoMan) {
		this.assoMan = assoMan;
	}

	public String getAssoTel() {
		return this.assoTel;
	}

	public void setAssoTel(String assoTel) {
		this.assoTel = assoTel;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	@Transient
	public int getOnlineNum() {
		return onlineNum;
	}

	public void setOnlineNum(int onlineNum) {
		this.onlineNum = onlineNum;
	}

	@Transient
	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
}
