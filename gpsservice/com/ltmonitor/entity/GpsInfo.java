﻿package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mongodb.ReflectionDBObject;

/**
 * GPS历史数据,保存GPS上传的实时定位信息，状态信息等
 * @author DELL
 *
 */

@Entity
@Table(name="gpsInfo")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class GpsInfo extends ReflectionDBObject implements Serializable
{
	public GpsInfo()
	{
		//setSendTime(new java.util.Date());
		setCreateDate(new java.util.Date());
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "gpsId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	private Date createDate ;
	
	
	//车终端卡号
	private String simNo;
	public final String getSimNo()
	{
		return simNo;
	}
	public final void setSimNo(String value)
	{
		simNo = value;
	}

	//车牌号
	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}
	
	//发送时间
	private java.util.Date sendTime = new java.util.Date(0);
	public final java.util.Date getSendTime()
	{
		return sendTime;
	}
	public final void setSendTime(java.util.Date value)
	{
		sendTime = value;
	}
	//经度
	private double longitude;
	public final double getLongitude()
	{
		return longitude;
	}
	public final void setLongitude(double value)
	{
		longitude = value;
	}
	//纬度
	private double latitude;
	public final double getLatitude()
	{
		return latitude;
	}
	public final void setLatitude(double value)
	{
		latitude = value;
	}
	//速度
	private double velocity;
	public final double getVelocity()
	{
		return velocity;
	}
	public final void setVelocity(double value)
	{
		velocity = value;
	}
	//对经纬度的地理位置解析
	private String location;
	public final String getLocation()
	{
		return location;
	}
	public final void setLocation(String value)
	{
		location = value;
	}
	//方向
	private int direction;
	public final int getDirection()
	{
		return direction;
	}
	public final void setDirection(int value)
	{
		direction = value;
	}
	//状态
	private int status;
	public final int getStatus()
	{
		return status;
	}
	public final void setStatus(int value)
	{
		status = value;
	}
	//报警位状态
	private int alarmState;
	public final int getAlarmState()
	{
		return alarmState;
	}
	public final void setAlarmState(int value)
	{
		alarmState = value;
	}
	//里程
	private double mileage;
	public final double getMileage()
	{
		return mileage;
	}
	public final void setMileage(double value)
	{
		mileage = value;
	}
	//油量
	private double gas;
	public final double getGas()
	{
		return gas;
	}
	public final void setGas(double value)
	{
		gas = value;
	}

	//行驶记录仪速度
	private double recordVelocity;
	public final double getRecordVelocity()
	{
		return recordVelocity;
	}
	public final void setRecordVelocity(double value)
	{
		recordVelocity = value;
	}

	private double altitude;
	
	private boolean valid;
	
	private String runStatus;
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getRunStatus() {
		return runStatus;
	}
	public void setRunStatus(String runStatus) {
		this.runStatus = runStatus;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


}