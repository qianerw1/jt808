﻿package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

//缓冲区的顶点
@Entity
@Table(name="LineBufferPoint")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class LineBufferPoint extends TenantEntity
{
	public LineBufferPoint()
	{
		setCreateDate(new java.util.Date());
	}

	public LineBufferPoint(int _enId, double lng, double lat)
	{
		setCreateDate(new java.util.Date());
		setLatitude(lat);
		setLongitude(lng);
		setEnclosureId(_enId);
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	private int nodeNo;
	
	private int enclosureId;
	public final int getEnclosureId()
	{
		return enclosureId;
	}
	public final void setEnclosureId(int value)
	{
		enclosureId = value;
	}
	//经度
	private double latitude;
	public final double getLatitude()
	{
		return latitude;
	}
	public final void setLatitude(double value)
	{
		latitude = value;
	}
	//纬度
	private double longitude;
	public final double getLongitude()
	{
		return longitude;
	}
	public final void setLongitude(double value)
	{
		longitude = value;
	}
	/**
	 * 没有实际意义，只是用来对点进行排序，
	 */
	private int sortNo;

	public int getNodeNo() {
		return nodeNo;
	}

	public void setNodeNo(int nodeNo) {
		this.nodeNo = nodeNo;
	}

	public int getSortNo() {
		return sortNo;
	}

	public void setSortNo(int sortNo) {
		this.sortNo = sortNo;
	}
}