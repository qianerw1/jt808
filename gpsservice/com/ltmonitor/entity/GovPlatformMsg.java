package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * 政府平台消息
 * @author DELL
 *
 */
@Entity
@Table(name="GovPlatformMsg")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class GovPlatformMsg extends TenantEntity{
	
	public static String TYPE_POST_QUERY = "查岗";//查岗
	
	public static String TYPE_MSG = "平台信息";//平台间信息
	
	public static String TYPE_WARN_URGE = "报警督办"; 
	
	private int entityId;
	
	private String plateNo;
	
	private String plateColor;
	
	private String msg;
	
	private String status;
	
	private String msgType;
	
	public GovPlatformMsg()
	{
		createDate = new Date();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "funcId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(String plateColor) {
		this.plateColor = plateColor;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
